export const $ = (selector)=>{
    if(selector[0]==='#')
    return document.getElementById(selector);
    else return document.querySelectorAll(selector);
}

export const createElement = (type, classList)=>{
    return (innerHTML)=>{
        const element =  document.createElement(type);
        element.classList.add(classList);
        element.innerHTML = innerHTML;
        return element;
    }
}