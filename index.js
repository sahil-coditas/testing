import reference from './reference.js';
import { createPopUp } from './components.js';
import { editPopUp } from './components.js';
import { deletePopUp } from './components.js';

const main = ()=>{
    const body = reference.body;
    const createButton = reference.createButton;
    createButton.onclick = ()=>{
       
        body.appendChild = popUp(createPopUp);
        const crossMark = reference.crossMark;
        crossMark.onClick = reference.body.removeChild(popUp(createPopUp));
    } 
    references.editButton.onClick = ()=>{
        body.appendChild = popUp(editPopUp);
        const crossMark = reference.crossMark;
        crossMark.onClick = body.removeChild(popUp(editPopUp));
    } 
    references.deleteButton.onClick = ()=>{
        body.appendChild = popUp(deletePopUp);
        const crossMark = reference.crossMark;
        crossMark.onClick = body.removeChild(popUp(deletePopUp));
    }
}

main();