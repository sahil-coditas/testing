import { $ } from "./dom.js";
const body = $('body');
const crossMark = $('#crossMark');
const createButton = $('#createButton');
const deleteButton = $('#deleteButton');
const editButton = $('#editButton');

export default{
    body, createButton, deleteButton, editButton, crossMark
}