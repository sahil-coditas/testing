import { createElement } from "./dom.js";

export const form = createElement('form', 'form');
export const popUp = createElement('div', 'popUp');

export const createTemplate = `
<div id="create-container">
<p>lorem50</p>
</div>
`;
export const createPopUp = form(createTemplate).outerHTML;

export const editTemplate = `
<div id="edit-container">
<p>lorem50</p>
</div>
`;
export const editPopUp = form(editTemplate).outerHTML;

export const deletePopUp = `
<div id="delete-container">
<p>lorem50</p>
</div>
`